FROM mhart/alpine-node:8

RUN npm -g config set user root
RUN npm install yamlinc@0.1.10 -g
RUN npm install spectacle-docs@1.0.7 -g

WORKDIR usr/swagger
COPY swagger.yaml usr/swagger/

CMD  yamlinc --watch spectacle -d swagger.yaml