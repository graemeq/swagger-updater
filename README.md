Install docker
Run docker-compose up

Navigate to: http://localhost:3200/index.html
This shows the current swagger.yaml file.


To Edit the file,
Edit either directly the swagger.yaml or link in the items you wish.

Use $include to link in other files.  See template for example.